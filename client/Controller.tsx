import React from 'react';
import {View, StyleSheet} from 'react-native';
import socketCluster from 'socketcluster-client';

import KEYS from './KEYS';
import ControllerButton from './ControllerButton';

interface ControllerProps {
  socketClient: socketCluster.AGClientSocket;
  errorHandler(e: Error): void;
}

const Controller = ({socketClient, errorHandler}: ControllerProps) => {
  const handleButtonPress = async (d: KEYS) => {
    let direction = await socketClient.invoke('keypress', d);
    if (/(UP)|(DOWN)|(RIGHT)|(LEFT)/.test(direction)) {
      setDirection(direction);
    } else {
      setDirection('Game Over');
      errorHandler(new Error('GO'));
    }
  };

  const [direction, setDirection] = React.useState<string>('RIGHT');

  return (
    <View style={styles.container}>
      <View style={styles.row}>
        <ControllerButton empty={true} />
        <ControllerButton
          label="▲"
          direction={KEYS.UP}
          empty={false}
          pressHandler={handleButtonPress}
        />
        <ControllerButton empty={true} />
      </View>
      <View style={styles.row}>
        <ControllerButton
          label="◀"
          direction={KEYS.LEFT}
          empty={false}
          pressHandler={handleButtonPress}
        />
        <ControllerButton empty={true} label={direction} />
        <ControllerButton
          label="▶"
          direction={KEYS.RIGHT}
          empty={false}
          pressHandler={handleButtonPress}
        />
      </View>
      <View style={styles.row}>
        <ControllerButton empty={true} />
        <ControllerButton
          label="▼"
          direction={KEYS.DOWN}
          empty={false}
          pressHandler={handleButtonPress}
        />
        <ControllerButton empty={true} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
    flex: 1,
  },
});

export default Controller;
