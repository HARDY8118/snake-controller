/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  StyleSheet,
  ToastAndroid,
  useColorScheme,
  SafeAreaView,
  TextInput,
  Button,
} from 'react-native';
import socketCluster from 'socketcluster-client';

import Controller from './Controller';
import checkIp from './checkIp';

const App = () => {
  const isDarkMode = useColorScheme() === 'dark';
  const [socket, setSocket] = React.useState<socketCluster.AGClientSocket>();
  const [connected, setConnectedState] = React.useState<boolean>(false);
  const [socketHost, onChangeSocketHost] =
    React.useState<string>('127.0.0.1:8000');
  const [buttonState, setButtonState] =
    React.useState<string>('Create connection');

  const createConnection = () => {
    if (checkIp(socketHost)) {
      setSocket(socketCluster.create({host: socketHost}));
      checkConnection();
      setButtonState('Connect');
    } else {
      setConnectedState(false);
      ToastAndroid.show('Not a valid host', ToastAndroid.SHORT);
    }
  };

  const checkConnection = async () => {
    try {
      ToastAndroid.show('Connecting', ToastAndroid.SHORT);
      if (socket && (await socket.invoke('health', null)) === 'OK') {
        setConnectedState(true);
        setButtonState('Connected');
      }
    } catch (e: any) {
      setConnectedState(false);
      console.error(e);
      ToastAndroid.show('Error', ToastAndroid.SHORT);
    }
  };

  const errorHandler = (e: Error) => {
    if (e.message == 'GO') {
      setSocket(undefined);
    } else {
      console.error(e);
    }
  };

  if (socket && connected) {
    return <Controller socketClient={socket} errorHandler={errorHandler} />;
  } else {
    return (
      <SafeAreaView >
        <TextInput
          value={socketHost}
          onChangeText={onChangeSocketHost}
          placeholder="Host [127.0.0.1:8000]"
          style={{borderWidth: 2, borderColor: '#0000ff', margin: 2}}
        />
        <Button title={buttonState} onPress={createConnection} />
      </SafeAreaView>
    );
  }
};

export default App;
