import React from 'react';
import {Text, StyleSheet} from 'react-native';
import KEYS from './KEYS';

interface ControlButtonProps {
  label?: string;
  direction?: KEYS;
  empty?: boolean;
  pressHandler?(dir: KEYS): void;
}

const ControllerButton = ({
  label,
  direction,
  empty,
  pressHandler,
}: ControlButtonProps) => {
  return empty ? (
    <Text style={styles.empty}>{label || ''}</Text>
  ) : (
    // @ts-ignore
    <Text style={styles.button} onPress={() => pressHandler(direction)}>
      {label}
    </Text>
  );
};

const styles = StyleSheet.create({
  empty: {
    backgroundColor: '#ffffff',
    flex: 1,
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  button: {
    backgroundColor: '#0000ff',
    flex: 1,
    textAlign: 'center',
    textAlignVertical: 'center',
    fontSize: 160,
  },
});

export default ControllerButton;
