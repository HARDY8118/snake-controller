A console based snake game with TypeScript and ReactNative android controller with SocketCluster RPC.

## server
```bash
cd server
tsc
node snake.js
```

## client
```bash
cd client
yarn start
```

```bash
cd client
yarn andrid
```

### Technologies used
#### server
- TypeScript
- SocketCluster-server

#### client
- React Native
- TypeScript
- SocketCluster-client