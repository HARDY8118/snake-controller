import * as http from "http";
import * as socketClusterServer from "socketcluster-server";
import * as events from "events";
import { networkInterfaces } from "os";

const httpServer = http.createServer();
const server = socketClusterServer.attach(httpServer, {});
const inputEmitter = new events.EventEmitter();
const PORT = 8000;

enum KEYS {
  UP,
  DOWN,
  RIGHT,
  LEFT,
}

const SNAKE = [
  [4, 4],
  [4, 3],
];
let dir = [0, 1];

let food: [number, number];
let foodCount = 0;

const ROWS = 20;
const COLS = 20;
let interval: NodeJS.Timer;

(async () => {
  for await (let { socket } of server.listener("connection")) {
    (async () => {
      for await (let req of socket.procedure("health")) {
        req.end("OK");
      }
    })();
    (async () => {
      for await (let req of socket.procedure("keypress")) {
        if (req.data!==null && req.data!==undefined) {
          switch (req.data) {
            case KEYS.UP: {
              if (dir[0] != 1) {
                dir = [-1, 0];
              }
              req.end("UP");
              break;
            }
            case KEYS.DOWN: {
              if (dir[0] != -1) {
                dir = [1, 0];
              }
              req.end("DOWN");
              break;
            }
            case KEYS.RIGHT: {
              if (dir[1] != -1) {
                dir = [0, 1];
              }
              req.end("RIGHT");
              break;
            }
            case KEYS.LEFT: {
              if (dir[1] != 1) {
                dir = [0, -1];
              }
              req.end("LEFT");
              break;
            }
            default:
              req.end("+");
          }
        } else {
          req.end("NULL");
        }
      }
    })();
  }
})();

function foodCreate() {
  food = [Math.floor(Math.random() * ROWS), Math.floor(Math.random() * COLS)];
  if (JSON.stringify(SNAKE).includes(JSON.stringify(food))) {
    foodCreate();
  }
}

function draw() {
  SNAKE.unshift([SNAKE[0][0] + dir[0], SNAKE[0][1] + dir[1]]);
  if (JSON.stringify(SNAKE.slice(1)).includes(JSON.stringify(SNAKE[0]))) {
    clearInterval(interval);
    process.stdout.write("\rOVER");
    process.exit();
  }

  if (SNAKE[0][0] == -1) {
    SNAKE[0][0] = ROWS - 1;
  }
  if (SNAKE[0][0] == ROWS) {
    SNAKE[0][0] = 0;
  }
  if (SNAKE[0][1] == -1) {
    SNAKE[0][1] = COLS - 1;
  }
  if (SNAKE[0][1] == COLS) {
    SNAKE[0][1] = 0;
  }
  if (SNAKE[0][0] != food[0] || SNAKE[0][1] != food[1]) {
    SNAKE.pop();
  } else {
    foodCreate();
    foodCount++;
  }

  process.stdout.write("\033c");
  for (let r = 0; r < ROWS; r++) {
    for (let c = 0; c < COLS; c++) {
      if (r == food[0] && c == food[1]) {
        process.stdout.write("**");
      } else if (JSON.stringify(SNAKE).includes(JSON.stringify([r, c]))) {
        // if(SNAKE.includes([r,c])){
        process.stdout.write("  ");
      } else {
        // process.stdout.write(`${r}${c}`)
        process.stdout.write("▇▇");
      }
    }
    process.stdout.write("\n");
  }
  // if (dir[0] == -1 && dir[1] == 0) {
  //   process.stdout.write("UP");
  // } else if (dir[0] == 1 && dir[1] == 0) {
  //   process.stdout.write("DOWN");
  // } else if (dir[0] == 0 && dir[1] == 1) {
  //   process.stdout.write("RIGHT");
  // } else if (dir[0] == 0 && dir[1] == -1) {
  //   process.stdout.write("LEFT");
  // }
}

httpServer.listen(PORT);

const ifaces = networkInterfaces();
process.stdout.write("Connection URIs\n");
for (let ifaceName of Object.keys(ifaces)) {
  // @ts-ignore
  for (const iface of ifaces[ifaceName]) {
    if (iface.family == "IPv4" && !iface.internal) {
      process.stdout.write(`${iface.address}:${PORT}\n`);
    }
  }
}
process.stdout.write("Press ENTER to continue");

process.stdin.once("data", (key) => {
  foodCreate();
  interval = setInterval(draw, 200);
});
